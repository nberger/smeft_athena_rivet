#!/bin/sh                                                                                                   
  
inFolder=/sps/atlas/e/erossi/Higgs_prod_SMEFT/Samples_tHW_01_5FS_par
outFolder=/sps/atlas/e/erossi/Higgs_prod_SMEFT/Rivet/Histograms100kEvts_tHW_01_5FS_par

mkdir $outFolder

prodMode=tHW

int=000

for par in 038 042
do
input=$inFolder/run_$par/EVNT.root
echo "$input"
output=$outFolder/${prodMode}_$par
tmpFolderName=${prodMode}_${par}

sbatch /sps/atlas/e/erossi/Higgs_prod_SMEFT/Rivet/runRivet_inTmpDir.sh $prodMode $input $output $tmpFolderName
done
