theApp.EvtMax = 1000
# theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool 
svcMgr.EventSelector.InputCollections = [inFileName] 

import os

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i

#histoFileName="histoFileName"
#histoFileName="rivetFile"
rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']
# rivet.Analyses += [ 'HiggsTemplateCrossSections' ]
rivet.Analyses += [ 'HiggsTemplateCrossSections:HiggsProdMode=%s:TupleFile=tuple.yaml' % higgsProdMode ]
rivet.RunName = ""
rivet.HistoFile = histoFileName
job += rivet

from GaudiSvc.GaudiSvcConf import THistSvc 
ServiceMgr += THistSvc() 
ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='"+histoFileName+".root' OPT='RECREATE'"]
#ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='"+histoFileName+".root' OPT='RECREATE'"]


