from array import array
import ROOT as rt
import yoda
import sys
from optparse import OptionParser

parser = OptionParser() 
parser.add_option("-f", "--yodaFile", help=".yoda file to be converted in root file", default="rivetFile.yoda", dest="yodaFile")
(options, args) = parser.parse_args()

fName = options.yodaFile
yodaAOs = yoda.read(fName)
rtFile = rt.TFile(fName[:fName.find('.yoda')] + '.root', 'recreate')

dirNames = list()

for name in yodaAOs:
  yodaAO = yodaAOs[name];  rtAO = None

  nameHist = name.rsplit('/',1)[1]
  #  if nameHist.count('[AUX_bare_not_for_analyses]'):
    
  if name.startswith('/'):
    name = name[1:]

  # Create TDirectories with same structure as in yoda names
  rtFile.cd()
  path = name.rsplit('/',1)[0]
  pathSaved = ""
  for dir in path.rsplit('/'):
    pathSaved += dir + "/"
    if not pathSaved in dirNames:
      rt.gDirectory.mkdir(dir)
      rtFile.cd(dir)
      dirNames.append(pathSaved)
  rtFile.cd()

  if 'Histo1D' in str(yodaAO):
    rtAO = rt.TH1D(nameHist, '', yodaAO.numBins(), array('d', yodaAO.xEdges()))
    rtAO.Sumw2(); rtErrs = rtAO.GetSumw2()
    for i in range(rtAO.GetNbinsX()):
      rtAO.SetBinContent(i + 1, yodaAO.bin(i).sumW())
      rtErrs.AddAt(yodaAO.bin(i).sumW2(), i+1)
  elif 'Scatter2D' in str(yodaAO):
    rtAO = rt.TGraphAsymmErrors(yodaAO.numPoints())
    for i in range(yodaAO.numPoints()):
      x = yodaAO.point(i).x(); y = yodaAO.point(i).y()
      xLo, xHi = yodaAO.point(i).xErrs()
      yLo, yHi = yodaAO.point(i).yErrs()
      rtAO.SetPoint(i, x, y)
      rtAO.SetPointError(i, xLo, xHi, yLo, yHi)
  else:
    continue
  rtFile.cd(name.rsplit('/',1)[0])
  rtAO.Write(nameHist)
  rtFile.cd()
rtFile.Close()
