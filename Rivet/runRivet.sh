#!/bin/sh 

rivet-build RivetHiggsTemplateCrossSections.so HiggsTemplateCrossSections.cc

mode=$1
input=$2
output=$3
athena.py -c 'inFileName="'$input'"; histoFileName="'$output'"; higgsProdMode="'$mode'";' RivetAnalysis.py
