#!/bin/sh 

path=/sps/atlas/e/erossi/Higgs_prod_SMEFT
outFolder=Samples_tHW_1_5FS_par
nevents=100000
prod=7 #tHW
int=000 #interference
source $path/setup.sh

for par in 038 042
do

mkdir $path/$outFolder/run_$par
cd $path/$outFolder/run_$par

cp $path/999999/HiggsTemplateCrossSectionsStage12.cc .
cp $path/999999/HiggsTemplateCrossSectionsStage12.h .

num=$prod$par$int

sbatch $path/genEvents.sh ''$num'' --get-user-env

cd $path
done
