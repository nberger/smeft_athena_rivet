# run the generation of SMEFT events + Rivet analysis routine
# arg1: run type, in the form NXXXYYY where 
#    - N is the run type (1=ggF, 2=VBF, 3=ZH, 4=WH, 5=ttH, 6=tHqb, 7=tHW)
#    - XXX is a topU3l Wilson coefficient index (1-123, see Generation/setEFTParam_topU3l.py for indexing scheme)
#    - YYY is another topU3l Wilson coefficient index.
#    - Both XXX and YYY coefficients are set to 1 if they are not 000.
# arg2: the number of events to generate
#
# Use cases
#  - Generate SM: N000000 (i.e. XXX=YYY=000) 
#  - Generate SM/SMEFT interference: NXXX000 (i.e. YYY=000, and XXX specifies the target Wilson coefficient, set to 1)
#  - Generate SMEFT^2 quadratic term: NXXXYYY for the term corresponding to XXX and YYY both set to 1.


Gen_tf.py --ecmEnergy=13000. --maxEvents=$2 --randomSeed=$1 --firstEvent=1 --outputEVNTFile=EVNT_$1.root --jobConfig=Generation |& tee log_$1_$2.txt


