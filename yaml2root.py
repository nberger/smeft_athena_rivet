#! /usr/bin/env python

import os, sys
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import yaml
import ROOT
from array import array

def make_parser() :
  parser = ArgumentParser("yaml2root.py", formatter_class=ArgumentDefaultsHelpFormatter)
  parser.description = __doc__
  parser.add_argument('input_file'      , type=str, nargs=1              , help='Name of the YAML input file')
  parser.add_argument('output_file'     , type=str, nargs='?', default='', help='Name of the ROOT output file')
  parser.add_argument('-b', '--branches', type=str           , default='', help='List of attributes to process')
  return parser

def run(argv = None) :
  parser = make_parser()
  options = parser.parse_args(argv)
  if not options :
    parser.print_help()
    sys.exit(0)

  try:
    print("INFO: opening file '%s'" % options.input_file[0]) 
    input_file = open(options.input_file[0], 'r')
  except IOError as exc:
    print(exc)
    raise IOError("Input file '%s' was not found" % options.input_file)
    sys.exit(1)

  data = yaml.safe_load(input_file)
  if len(data) == 0 :
    print('INFO: input file is empty, exiting')
    sys.exit(0)

  if options.branches == '' :
    first_entry = data[0]
    try:
      branches = list(first_entry.keys())
    except Execption as exc :
      print(exc)
      raise ValueError("First entry '%' in the data was not a dictionary as expected" % str(first_entry))
    print('Storing attributes %s' % str(branches))
  else :
    branches = options.branches.split(':')
  
  output_name = options.output_file if options.output_file != '' else options.input_file[0][:-4] + 'root'
  output_file = ROOT.TFile(output_name, 'RECREATE')
  tree = ROOT.TTree('tree', '')
  leaves = []

  for i, branch in enumerate(branches) :
    leaf = array('f', [0.])
    tree.Branch(branch, leaf, branch + '/F')
    leaves.append(leaf)    
    
  for k, entry in enumerate(data) :
    for i, branch in enumerate(branches) :
      if not branch in entry :
        raise KeyError("Attribute '%s' not found in entry %d (full dump below), exiting.\n%s" % (branch, k, str(entry)))
      leaves[i][0] = entry[branch]
    tree.Fill()
  tree.Write()
  output_file.Close()
  sys.exit(0)

if __name__ == '__main__' : run()
