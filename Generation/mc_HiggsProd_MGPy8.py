evgenConfig.keywords    = [ 'Higgs' ]
evgenConfig.contact     = [ 'nicolas.berger@cern.ch' ] 
evgenConfig.generators  = [ 'MadGraph', 'Pythia8', 'EvtGen' ]
evgenConfig.description = """Leading order SMEFT samples for Higgs production; STXS stage 1.2 classification"""

from MadGraphControl.MadGraphUtils import *
from os.path import join as pathjoin 
import os
import sys
from Rivet_i.Rivet_iConf import Rivet_i

safe_factor=2.
nevents=safe_factor*runArgs.maxEvents

ci_val=1.0 # default
if runArgs.firstEvent == 2:
    ci_val=-0.5
elif runArgs.firstEvent == 3:
    ci_val=0.1

channel=runArgs.randomSeed


#==================================================================================

# Merging parameters in MadGraph. removed xqcut
ickkw=0

ptj=20.0
ptb=20.0

ptl=0.0
etal=10

drll=0.05
drjj=0.1
drbb=0.1
drbj=0.1

pdfwgt='T'

# Additional merging parameters in Pythia
ktdurham=30
dparameter=0.4
nJetMax=2 
physProcess='pp>h'

maxjetflavor=5  # should be 5 if b included;

mbottom=0
ymb=0


#==================================================================================

if len(str(channel)) != 7:
    raise RuntimeError("Not a valid run number: must have 7 digits (first digit = production mode; last 2 pairs: XXXYYY=BSM (19-82), XXX000=interference, 000000=SM)")

#==================================================================================
# Set SM or interference or BSM term; SM run number ends with 00
NPmode=''
if channel % 1000000 == 0: 
    print "Simulate SM term"
    NPmode='NP^2==0 NP=1'
elif channel % 1000 == 0:
    print "Simulate interference term"
    NPmode='NP^2==1 NP=1'
else:
    print "Simulate BSM term"
    NPmode='NP^2==2 NP=1'

if len(NPmode)==0:
    raise RuntimeError("Do not recognise which term of the cross section to generate (SM, interference or BSM)")

#==================================================================================
# Write process according to run number
if str(channel)[:1] == '1': # ggH+bbH
    print "Generate ggH+bbH events."
    processCommand="""
generate p p > h QED=1 """+NPmode+""" @0
add process p p > h j QED=1 """+NPmode+""" @1
add process p p > h j j QED=1 """+NPmode+""" @2
add process p p > h b b~ QED=1 """+NPmode+""" @3
    """

elif str(channel)[:1] == '2': # VBF+VHhad
    print "Generate VBF+VHhad events."
    processCommand="""
generate p p > h j j QCD=0 """+NPmode+""" @0
    """

elif str(channel)[:1] == '3': # ZHlep
    print "Generate ZHlep events."
    processCommand="""
generate p p > h l+ l- """+NPmode+""" @0
add process p p > h ta+ ta- """+NPmode+""" @1
add process p p > h vl vl~ """+NPmode+""" @2
    """

elif str(channel)[:1] == '4': # WHlep
    print "Generate WHlep events."
    processCommand="""
generate p p > h l+ vl """+NPmode+""" @0
add process p p > h l- vl~ """+NPmode+""" @1
    """

elif str(channel)[:1] == '5': # ttH
    print "Generate ttH events."
    processCommand="""
generate p p > h t t~ """+NPmode+""" @0
    """
 
elif str(channel)[:1] == '6': # tHjb
    print "Generate tHjb events."
    processCommand="""
    generate p p > h t b~ j """+NPmode+""" @0
    add process p p > h t~ b j """+NPmode+""" @1

    """
elif str(channel)[:1] == '7': # tHW
    print "Generate tHW events."
    processCommand="""
generate p p > h t w- """+NPmode+""" @0
add process p p > h t~ w+ """+NPmode+""" @1
    """

else:
    raise RuntimeError("Not a valid production mode (first digit of run number 1-5)")

process = ("""
import model ./SMEFTsim_topU3l_MwScheme_UFO-massless_prod
set gauge unitary
define p = p b b~
define j = p"""
+processCommand+"""
output %s/PROC_SMEFTsim_topU3l_MwScheme_UFO-massless_prod-%s -f
""" % (os.getcwd(), channel))

#define p = p b b~  removed for 4FS

#==================================================================================

# Which parameters to modify (default: 0)
execfile('Generation/set_SMEFT_topU3l.py')
smeft_params = {}
smeft_cpv_params = {}
set_SMEFT(int(str(channel)[1:4]), 1.0, smeft_params, smeft_cpv_params)
set_SMEFT(int(str(channel)[4:7]), 1.0, smeft_params, smeft_cpv_params)
print(smeft_params)
print(smeft_cpv_params)


#==================================================================================

# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#==================================================================================

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'pdlabel':'lhapdf',
             'lhaid':'90400',
             'drll'        : drll,
             'drjj'        : drjj,
             'drbb'        : drbb,
             'drbj'        : drbj,
             'ptj'         : ptj,
             'ptb'         : ptb,
             'ptl'         : ptl,
             'etal'        : etal,
             'pdfwgt'      : pdfwgt,
             'maxjetflavor': maxjetflavor,
             'use_syst'    : "False",
             'nevents'     : int(nevents),
             'sde_strategy': 1,
             'beamEnergy'  : beamEnergy,
}

#==================================================================================
process_dir = new_process(process, keepJpegs=True)

#==================================================================================

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#==================================================================================

param_card = open('param_card.dat','w')
param_card_default = open('SMEFTsim_topU3l_MwScheme_UFO/param_card_default.dat','r')

SMEFT={
    'cG':0,
    'cW':0,
    'cH':0,
    'cHbox':0,
    'cHDD':0,
    'cHG':0,
    'cHW':0,
    'cHB':0,
    'cHWB':0,
    'cuHRe':0,
    'ctHRe':0,
    'cdHRe':0,
    'cbHRe':0,
    'cuGRe':0,
    'ctGRe':0,
    'cuWRe':0,
    'ctWRe':0,
    'cuBRe':0,
    'ctBRe':0,
    'cdGRe':0,
    'cbGRe':0,
    'cdWRe':0,
    'cbWRe':0,
    'cdBRe':0,
    'cbBRe':0,
    'cHj1':0,
    'cHQ1':0,
    'cHj3':0,
    'cHQ3':0,
    'cHu':0,
    'cHt':0,
    'cHd':0,
    'cHbq':0,
    'cHudRe':0,
    'cHtbRe':0,
    'cjj11':0,
    'cjj18':0,
    'cjj31':0,
    'cjj38':0,
    'cQj11':0,
    'cQj18':0,
    'cQj31':0,
    'cQj38':0,
    'cQQ1':0,
    'cQQ8':0,
    'cuu1':0,
    'cuu8':0,
    'ctt':0,
    'ctu1':0,
    'ctu8':0,
    'cdd1':0,
    'cdd8':0,
    'cbb':0,
    'cbd1':0,
    'cbd8':0,
    'cud1':0,
    'ctb1':0,
    'ctd1':0,
    'cbu1':0,
    'cud8':0,
    'ctb8':0,
    'ctd8':0,
    'cbu8':0,
    'cutbd1Re':0,
    'cutbd8Re':0,
    'cju1':0,
    'cQu1':0,
    'cju8':0,
    'cQu8':0,
    'ctj1':0,
    'ctj8':0,
    'cQt1':0,
    'cQt8':0,
    'cjd1':0,
    'cjd8':0,
    'cQd1':0,
    'cQd8':0,
    'cbj1':0,
    'cbj8':0,
    'cQb1':0,
    'cQb8':0,
    'cjQtu1Re': 0,
    'cjQbd1Re':0,
    'cjQbd8Re':0,
    'cjujd1Re':0,
    'cjujd8Re':0,
    'cjujd11Re':0,
    'cjujd81Re':0,
    'cQtjd1Re':0,
    'cQtjd8Re':0,
    'cjuQb1Re':0,
    'cjuQb8Re':0,
    'cQujb1Re':0,
    'cQujb8Re':0,
    'cjtQd1Re':0,
    'cjtQd8Re':0,
    'cQtQb1Re':0,
    'cQtQb8Re':0,
    'ceHRe':0,
    'ceWRe':0,
    'ceBRe':0,
    'cHl1':0,
    'cHl3':0,
    'cHe':0,
    'cll':0,
    'cll1':0,
    'clj1':0,
    'clj3':0,
    'cQl1':0,
    'cQl3':0,
    'cee':0,
    'ceu':0,
    'cte':0,
    'ced':0,
    'cbe':0,
    'cje':0,
    'cQe':0,
    'clu':0,
    'ctl':0,
    'cld':0,
    'cbl':0,
    'cle':0,
    'cledjRe':0,
    'clebQRe':0,
    'cleju1Re':0,
    'cleQt1Re':0,
    'cleju3Re':0,
    'cleQt3Re':0,
}

SMEFTcpv = {  
   'cGtil'     : 0.,
   'cWtil'     : 0.,
   'cHGtil'    : 0.,
   'cHWtil'    : 0.,
   'cHBtil'    : 0.,
   'cHWBtil'   : 0.,
   'cuGIm'     : 0.,
   'ctGIm'     : 0.,
   'cuWIm'     : 0.,
   'ctWIm'     : 0.,
   'cuBIm'     : 0.,
   'ctBIm'     : 0.,
   'cdGIm'     : 0.,
   'cbGIm'     : 0.,
   'cdWIm'     : 0.,
   'cbWIm'     : 0.,
   'cdBIm'     : 0.,
   'cbBIm'     : 0.,
   'cuHIm'     : 0.,
   'ctHIm'     : 0.,
   'cdHIm'     : 0.,
   'cbHIm'     : 0.,
   'cHudIm'    : 0.,
   'cHtbIm'    : 0.,
   'cutbd1Im'  : 0.,
   'cutbd8Im'  : 0.,
   'cjQtu1Im'  : 0.,
   'cjQtu8Im'  : 0.,
   'cjQbd1Im'  : 0.,
   'cjQbd8Im'  : 0.,
   'cjujd1Im'  : 0.,
   'cjujd8Im'  : 0.,
   'cjujd11Im' : 0.,
   'cjujd81Im' : 0.,
   'cQtjd1Im'  : 0.,
   'cQtjd8Im'  : 0.,
   'cjuQb1Im'  : 0.,
   'cjuQb8Im'  : 0.,
   'cQujb1Im'  : 0.,
   'cQujb8Im'  : 0.,
   'cjtQd1Im'  : 0.,
   'cjtQd8Im'  : 0.,
   'cQtQb1Im'  : 0.,
   'cQtQb8Im'  : 0.,
   'ceHIm'     : 0.,
   'ceWIm'     : 0.,
   'ceBIm'     : 0.,
   'cledjIm'   : 0.,
   'clebQIm'   : 0.,
   'cleju1Im'  : 0.,
   'cleju3Im'  : 0.,
   'cleQt1Im'  : 0.,
   'cleQt3Im'  : 0.
}

SMEFT.update(smeft_params)
SMEFTcpv.update(smeft_cpv_params)

yukawa={'ymb' : ymb}
mass={'MB' : mbottom}

params={}
params['mass']=mass
params['SMEFT']=SMEFT
params['SMEFTcpv']=SMEFTcpv
print(params)
modify_param_card(process_dir=process_dir,params=params)

param_card.close()
param_card_default.close()

#==================================================================================

print_cards()


print('### ###', os.getcwd())
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#==================================================================================

# Shower 

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

if str(channel)[:1] == '1':
    PYTHIA8_nJetMax=nJetMax
    PYTHIA8_Dparameter=dparameter
    PYTHIA8_Process=physProcess
    PYTHIA8_TMS=ktdurham
    PYTHIA8_nQuarksMerge=maxjetflavor
    include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
    genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]

if str(channel)[:1] == '1': # ggH+bbH
    higgsProdMode = "GGF"
elif str(channel)[:1] == '2':# VBF
    higgsProdMode = "VBF"
elif str(channel)[:1] == '3':# ZHlep
    higgsProdMode = "QQ2ZH"
elif str(channel)[:1] == '4':# WHlep
    higgsProdMode = "WH"
elif str(channel)[:1] == '5':# TTH
    higgsProdMode = "TTH"
else: #TH and THW                                                                                                           
    higgsProdMode = "TH"


histoFileName="rivetFile"

genSeq += Rivet_i()
genSeq.Rivet_i.AnalysisPath = os.path.join(os.environ['PWD'], 'Rivet')
genSeq.Rivet_i.Analyses += [ 'HiggsTemplateCrossSections:HiggsProdMode=%s:TupleFile=tuple_%d.yaml' % (higgsProdMode, channel) ]
genSeq.Rivet_i.RunName = ""
genSeq.Rivet_i.HistoFile = histoFileName

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
