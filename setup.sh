#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

asetup 21.6.75,AthGeneration #this is what we currently use for the Global combination, updates on Athena can be found here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PmgMcSoftware

source setupRivet.sh #this is Rivet 3.1.4
